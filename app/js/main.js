$(document).ready(function() {
    // Modale
    var modale_triggers = $(".jsToggleModale"),
        modale_close = $(".jsCloseModale"),
        modale_shadows = $(".jsModaleShadow");

    modale_triggers.click(function (e) {
        var target = document.getElementById($(e.target).attr("data-modale"));
        e.preventDefault();
        $(target).toggleClass("hidden");
        $(target).next(".jsModaleShadow").toggleClass("hidden");
    });

    modale_shadows.click(function (e) {
        $(this).toggleClass("hidden");
        $(this).prev(".jsModale").toggleClass("hidden");
    });

    modale_close.click(function (e) {
        $(this).parent().next(".jsModaleShadow").toggleClass("hidden");
        $(this).parent(".jsModale").toggleClass("hidden");
    });


    // Nom document transmis en input hiddem
    $(".jsDocument").click(function (e) {
        $('#jsDocumentInput').val($(this).parent().prev().text())
    });

    // Règles gestions form
    $('input.jsCheckInput').blur(function (e) {
        $(this).next('.jsErrorMessage').find('.jsEmptyMsg, .jsNotValidMsg').hide()
        if ($(this).val() == "") {
            $(this).next('.jsErrorMessage').find('.jsEmptyMsg').show()
        }
        else if ($(this).hasClass('jsCheckEmail')) {
            if (!isValidEmailAddress($(this).val())) {
                $(this).next('.jsErrorMessage').find('.jsNotValidMsg').show()
            }
        }
    });

    function isValidEmailAddress(emailAddress) {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        return pattern.test(emailAddress);
    };
});