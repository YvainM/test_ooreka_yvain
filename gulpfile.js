var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('sass', function(){
	return gulp.src('app/sass/styles.sass')
		.pipe(sass({outputStyle: 'compressed'}))
		.pipe(gulp.dest('app/css'))
});

gulp.task('watch', function(){
	gulp.watch(['app/sass/styles.sass'], ['sass']);
})